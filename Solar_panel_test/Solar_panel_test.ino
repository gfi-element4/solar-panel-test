#include <AD5231.h> // SPI library is already included in this library so no need to add it a second time
AD5231 MyDigiPot(5); // slave                                                                                                          select on pin 5
const int voltagePin = A0;
//int value;

//float Rmax = 8700; //MAXIMUM RESISTANCE
//float Rmin = 74; //WIPER RESISTANCE (MINIMUM)
//float R_Interval = (Rmax - Rmin) / 1024 ;



float readAvg(int n) {
  float norm = 1. / n;
  float out = 0;
  for (int i = 0; i < n; i++)
    out += analogRead(voltagePin) * norm;

  return out;
}

void printValues(int R) {
  // read value from analog pin
  // value = analogRead(voltagePin);
  float value = readAvg(10);
  // Write the result
  
  if (Serial.availableForWrite()) {
    String outstr = String(String(R) + "," + String(value));
    delay(100);
    Serial.println(outstr);
 
  }
}

void setup() {
  MyDigiPot.begin(); // SPI.begin is taken care of in this initilisation
  Serial.begin(115200);
  while (!Serial) 
  {
    ; // wait for serial port to connect
  }
  
  MyDigiPot.WriteWiper(512);//midscale
  delay(500);

}

void loop() {
  
  
  if (Serial.available())
  { 
    String ADCString = Serial.readStringUntil('\n');
    long adc = ADCString.toInt(); //Desired resistance
    

    MyDigiPot.WriteWiper(adc);
    delay(100);
    MyDigiPot.WriteWiper(adc);
    delay(500);

    
   float  value = readAvg(10);
   Serial.println(String(adc) + "," + String(value));
    
//    printValues(adc);
  }
}
